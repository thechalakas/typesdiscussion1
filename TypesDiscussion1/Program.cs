﻿using System;

namespace TypesDiscussion1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //create an instance of the Purple class
            Purple purple = new Purple();

            purple.saysomething();

            //var CurrentUser = UserManager.Users.First(u => u.UserName.Equals(UserName));

            //Points of Interest

            //1st - var - done
            //2nd - Currentuser - done
            //3rd - UserManager - namespace - done
            //4th - u - ???? - ????  anonymous type
            //5th - => --?? - 
            //6th - u.UserName.Equals
            //7th - UserName

            int a = 10;
              var somestring = purple.returnnamefromdb();
            //var CurrentUser = UserManager.Users.First(u => u.UserName.Equals(UserName));
            //UserManager.Users.First(u => u.UserName.Equals(UserName));

            //u => u.UserName.Equals(UserName)

            //the important thing is 
            //only 25 characters
            //1 line of coade

            //12 to 15 lines
            //with some 100 to 200 characters.

            //imagine a world where anonymous methods and anonymous types did not exist

            // Purple u = new Purple();
            //also please understand that we know that u is Purple in advance of time. 
            //which means, it is your responsibility to find out what type is u is

            //= and > are not two symbols.
            //lambda 



            //Equals method.
            Console.WriteLine("value of a is {0}", a);

            Console.ReadLine();
        }

        public Purple Equals(String UserName)
        {
            //do all the code related calculations.
            //like LINQ queries.
            //get the results
            //return the results.
            //total may be about 5 to 10 lines.

            Purple temp = new Purple();
            return temp;
        }
    }

    //a simple standard Type Definition
    //class definition
    //this took about 8 minutes total to write and use. 

    public class Purple
    {
        public Purple()
        {
                   
        }

        public void saysomething()
        {
            Console.WriteLine("Hello I am saying Purple");
        }

        public string returnnamefromdb()
        {
            string dummydata = "this is some dummy data";
            return dummydata;
        }
    }
}